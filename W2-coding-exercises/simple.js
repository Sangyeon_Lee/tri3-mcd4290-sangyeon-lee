//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let array = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,-51, -17, -25];
    let PositiveOdd = [];
    let NegativeEven = [];
    for(let i = 0; i <= array.length; i++){
        if (array[i] > 0 && array[i] % 2 != 0){
            PositiveOdd.push(array[i])
            }
        else if (array[i] < 0 && array[i] % 2 == 0){
            NegativeEven.push(array[i])
            }
    }

    output += "Positive Odd: " + PositiveOdd + "\n";
    output += "Negative Even: " + NegativeEven
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1" 
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    let count1 = 0
    let count2 = 0
    let count3 = 0
    let count4 = 0
    let count5 = 0
    let count6 = 0
    //Question 2 here 
       
    for(let i = 0; i < 60000; i++){
        let value = Math.floor((Math.random() * 6) + 1) 
        
        if ( value == 1)
            count1++
        else if ( value == 2)
            count2++
        else if ( value == 3)
            count3++
        else if (value == 4)
            count4++
        else if (value == 5)
            count5++
        else
            count6++
    }
    output = "Frequency of die rolls" + "\n"
    output += "1: " + count1 + "\n"
    output += "2: " + count2 + "\n"
    output += "3: " + count3 + "\n"
    output += "4: " + count4 + "\n"
    output += "5: " + count5 + "\n"
    output += "6: " + count6 

    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    let array = [0,0,0,0,0,0,0];
    
    //Question 3 here 
    for (let i = 0; i < 60000; i++){
        array[Math.floor((Math.random() *6) + 1)]++;
        
    }
    
    output = "Frequency of die rolls" + "\n"
    for (let j = 1; j < array.length; j++ ){
        output += j + ": " + array[j] + "\n" 
        }
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
            },
        Total:60000,
        Exceptions: ""
    }
    
    for (let i = 0; i <= dieRolls.Total; i++){
        dieRolls.Frequencies[Math.floor((Math.random() *6) + 1)]++;
    }
    
    output = "Frequency of dice rolls" + "\n";
    output = "Total rolls: " + dieRolls.Total + "\n" ;
    output = "Frequencies: " + "\n"
    for (prop in dieRolls.Frequencies){
        
        output += prop + ": " + dieRolls.Frequencies[prop] + "\n" ;
        
        if (dieRolls.Frequencies[prop] < 9900 || dieRolls.Frequencies[prop] > 10100)
            dieRolls.Exceptions += prop
       
    }
    output  += "Exceptions: " + dieRolls.Exceptions + "\n"
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    let person = {
        name: "Jane",
        income: 127050
    }
    
    let tax = 0;
    //Question 5 here 
    if (person.income < 18200)
        tax = 0
    else if (person.income>18201 && person.income<37000)
        tax = 0.19 * (person.income - 18200)
    else if (person.income>37001 && person.income<90000)
        tax = 0.325 * (person.income - 37000)
    else if (person.income>90001 && person.income<180000)
        tax = 0.37 * (person.income - 90000)
    else if (person.income>180001)
        tax = 0.45 * (person.income - 180000)
        
    output = person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + tax 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}